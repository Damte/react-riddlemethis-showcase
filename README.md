# Riddle Me This frontend

* This is a project based on the final project made during my studies with SDA (in a group of 4 people). Back then everything was done with vanilla Javascript. I took the chance to redo the project in Reactjs as a way to better put in practice with a real example what I have been learning. The work is still ongoing, currently I'm working with another colleague on it in order to change some aspects of the project and add some functionalities and improvements.

* This project is currently in process of improvement. Because of that it is possible that in the source code still can be found duplications and incoherences. Also some refactoring and cleaning work is still needed in order to make the code more readable and better structured. Since this has been a learning project of personal nature and that implied continuos changes this process has been a bit postponed to a later stage of the development.

* In the near future the navigation and routing (as well as some component's behaviour) will be optimised so that the app can be run and used on the browser. Currently it is only partially working and this repository just contains a sample of the code made by 03-03-2021. 

* Backend for running this project can be found in here: https://bitbucket.org/Damte/showcase-simplebankapp/src/master/

