import React from "react";
import Carousel from "react-bootstrap/Carousel";
import HeaderBackground from "../images/header-bg-1.jpg";
import "./Carousel.css";

function NewCarousel() {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={HeaderBackground}
          alt="First slide"
        />
        <Carousel.Caption>
          <div className="slider-content">
            <div className="col-md-12 text-center">
              <h1 className="animated3">
                <span>
                  Ask <strong>questions</strong> with RMT
                </span>
              </h1>
              <p className="animated2">
                Create your own quizzes, polls or surveys for sharing with your
                friends and colleagues
              </p>
              <a
                href="#feature"
                className="page-scroll btn btn-primary animated1"
              >
                Read More
              </a>
            </div>
          </div>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src={HeaderBackground}
          alt="Third slide"
        />

        <Carousel.Caption>
          <div className="slider-content">
            <div className="col-md-12 text-center">
              <h1 className="animated2">
                <span>
                  Step in and <strong>get started</strong>
                </span>
              </h1>
              <p className="animated1">
                Sign up, create and <br /> share your customized quizzes
              </p>
              <a
                className="animated3 slider btn btn-primary btn-min-block"
                href="#call-to-action"
              >
                Sign Up
              </a>
              <a
                className="animated3 slider btn btn-default btn-min-block"
                href="#"
              >
                Login
              </a>
            </div>
          </div>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default NewCarousel;
