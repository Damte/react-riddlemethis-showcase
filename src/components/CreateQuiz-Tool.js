import React, { useState, useEffect, useContext } from "react";
import QuizName from "./CreateQuiz-QuizName";
import Question from "./CreateQuiz-Question";
import LoginForm2 from "./LoginForm2";
import useCreateQuizForm from "./validation/useForm2";
import validate from "./validation/CreateQuizValidationRules";
// import { Context } from "./IsAuthenticatedContext";
function QuizCreationTool() {
  //   const [context, setContext] = useContext(Context);
  const [userId, setUserId] = useState(1);
  const [questNum, setQuestNum] = useState(3);
  const [questNumEdited, setQuestNumEdited] = useState(1);
  const [quizName, setQuizName] = useState("");
  const [submitEvent, setSubmitEvent] = useState("");
  const [questionDTOs, setQuestionDTOs] = useState([]);
  const [hasSubmitted, setHasSubmitted] = useState(false);
  const [questionComponants, setQuestionComponants] = useState([
    <div>
      <Question
        key={1}
        questionNum={1}
        onChange={handleQuestionChange}
        hasSubmitted={hasSubmitted}
        event={submitEvent}
      ></Question>
    </div>,
    <div>
      <Question
        key={2}
        questionNum={2}
        onChange={handleQuestionChange}
        hasSubmitted={hasSubmitted}
        event={submitEvent}
      ></Question>
    </div>,
  ]);
  const [quizDTO, setQuizDTO] = useState({
    listOfQuestionDTOs: questionDTOs,
    name: "",
    userId: 0,
  });
  const { values, errors, handleChange, handleSubmit } = useCreateQuizForm(
    enterData,
    validate
  );
  function enterData() {
    console.log("No errors, submit callback called!");
  }

  function handleQuizNameChange(newName, savedEvent) {
    setQuizName(newName);
    setQuizDTO({ ...quizDTO, name: quizName });
    // setSavedEvent1(savedEvent)
    handleChange(savedEvent);
  }

  function validateQuizName(event) {
    // handleChange(event)
  }

  useEffect(() => {
    console.log(`The quizDTO: ${JSON.stringify(quizDTO)}`);
  }, [quizDTO]);
  useEffect(() => {
    setQuizDTO({
      ...quizDTO,
      listOfQuestionDTOs: questionDTOs,
      userId: userId,
    });
    console.log(`quiz DTO2: ${JSON.stringify(quizDTO)}`);
  }, [questionDTOs]);
  function handleQuestionChange(newDTO, questNum, savedEvent) {
    let currentQuestionNumber = questNum - 1;
    if (questionDTOs.length < currentQuestionNumber) {
      questionDTOs.push({});
      currentQuestionNumber++;
    }
    questionDTOs.splice(currentQuestionNumber, 1, newDTO);
    let newQuestDTOs = JSON.parse(JSON.stringify(questionDTOs));
    setQuestionDTOs(newQuestDTOs);
    console.log(`questionDTOs: ${JSON.stringify(questionDTOs)}`);
    // setSavedEvent1(savedEvent)
    // handleChange(savedEvent)
    // handleChange(savedEvent1)
  }

  useEffect(() => {
    console.log(`The quizDTO: ${JSON.stringify(quizDTO)}`);
  }, [quizDTO]);

  useEffect(() => {
    setQuizDTO({
      ...quizDTO,
      listOfQuestionDTOs: questionDTOs,
      userId: userId,
    });
    console.log(`quiz DTO2: ${JSON.stringify(quizDTO)}`);
  }, [questionDTOs]);

  function addQuestion() {
    setQuestNum(questNum + 1);
    const addOne = [
      ...questionComponants,
      <Question
        key={questNum}
        questionNum={questNum}
        onChange={handleQuestionChange}
        hasSubmitted={hasSubmitted}
        event={submitEvent}
      ></Question>,
    ];
    setQuestionComponants(addOne);
  }

  // State and function for the 'Remove Question' button, disables the button when there are less than 2 question comonents
  //(the number 3 is used, as the saved questions number state is always one number ahead)
  const [buttonRemQuestIsDisabled, setButtonRemQuestIsDisabled] = useState({});
  useEffect(() => {
    setButtonRemQuestIsDisabled(questNum <= 3);
  }, [questNum]);
  function remQuestion() {
    setQuestNum(questNum - 1);
    questionComponants.pop();
    setQuestionComponants(questionComponants);
  }

  //----------------------------------------------------------------------------------
  function handleSubmitEvent(event) {
    setSubmitEvent(event);
    setHasSubmitted(true);
    handleSubmit(event);
  }

  return (
    <div>
      <form onSubmit={handleSubmitEvent} noValidate>
        <div>
          <QuizName
            onChange={handleQuizNameChange}
            passEvent={validateQuizName}
            errors={errors}
          ></QuizName>
        </div>
        {questionComponants.map((question) => (
          <div>{question}</div>
        ))}

        <button
          className="form-control btn btn-primary"
          id="qct-q1-addAns"
          onClick={addQuestion}
        >
          Add question
        </button>
        <button
          className="btn btn-primary"
          id="qct-q1-remAns"
          onClick={remQuestion}
          disabled={buttonRemQuestIsDisabled}
        >
          Remove question
        </button>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
}
export default QuizCreationTool;

// return (
//   <div>
//     <form noValidate>
//       <div>
//         <QuizName
//           onChange={handleQuizNameChange}
//           errorLog={errors}>
//         </QuizName>
//       </div>
//       {questionComponants.map((question) =>
//       (<div>
//         {<Question
//           key={questNum}
//           questionNum={questNum}
//           onChange={handleQuestionChange}
//           errorLog={errors}
//           />}
//       </div>))}
//       <button
//         className="form-control btn btn-primary"
//         id="qct-q1-addAns"
//         onClick={addQuestion}>
//         Add question
//           </button>
//       <button
//         className="btn btn-primary"
//         id="qct-q1-remAns"
//         onClick={remQuestion}
//         disabled={buttonRemQuestIsDisabled}>
//         Remove question
//       </button>
//       <button type="submit" className="btn btn-primary" onClick={handleSubmit}>Submit</button>
//     </form>
//   </div>
// );
// }
// export default QuizCreationTool;
