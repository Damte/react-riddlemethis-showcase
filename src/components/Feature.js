import React from "react";
import "./Feature.css";

function Feature() {
  return (
    <>
      <section id="feature" className="feature-section">
        <div className="container">
          <div className="row">
            <div className="col-md-3 col-sm-6 col-xs-12">
              <div className="feature">
                <i className="fa fa-list" aria-hidden="false"></i>
                <div className="feature-content">
                  <h3>Quizzes</h3>
                  <p style={{ textAlign: "justify" }}>
                    Test the knowledge of your peers in any specific topic of
                    your choice by creating a set of questions and answers.
                  </p>
                </div>
              </div>
            </div>
            {/* <!-- /.col-md-3 --> */}
            <div className="col-md-3 col-sm-6 col-xs-12">
              <div className="feature">
                <i className="fa fa-pencil" aria-hidden="true"></i>
                <div className="feature-content">
                  <h3>Polls</h3>
                  <p style={{ textAlign: "justify" }}>
                    Sound out opinions and collect general data on trends or
                    patterns of opinion on a societal level, later on you can
                    review and share the data via visual representations
                  </p>
                </div>
              </div>
            </div>
            {/* <!-- /.col-md-3 --> */}
            <div className="col-md-3 col-sm-6 col-xs-12">
              <div className="feature">
                <i className="fa fa-pie-chart" aria-hidden="true"></i>
                <div className="feature-content">
                  <h3>Surveys</h3>
                  <p style={{ textAlign: "justify" }}>
                    Conduct more elaborated opinion analisis by using our
                    Surveys, this provides a different set of tools to collect
                    feedback from your target audience
                  </p>
                </div>
              </div>
            </div>
            {/* <!-- /.col-md-3 --> */}
            <div className="col-md-3 col-sm-6 col-xs-12">
              <div className="feature">
                <i className="fa fa-line-chart" aria-hidden="true"></i>
                <div className="feature-content">
                  <h3>Statistics</h3>
                  <p style={{ textAlign: "justify" }}>
                    Consult statistics about any kind of quiz, poll or survey
                    that you conduct and review it with others by using the
                    charted or graphed percentages tools available. Customize
                    your graphic representation and share it with the world!
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- /.row --> */}
        </div>
        {/* <!-- /.container --> */}
      </section>
      {/* <!-- End Feature Section --> */}
    </>
  );
}

export default Feature;
