import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FormLabel from "react-bootstrap/FormLabel";
import "./ContactUs.css";

function ContactUs() {
  return (
    <>
      <section className="buy-pro" style={{ padding: "100px 0" }}>
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2">
              <div
                className="pro-block text-center"
                style={{
                  padding: "40px 50px",
                  background: "white",
                  boxShadow: "0px 2px 28px 0px #7777775e",
                  borderRadius: "5px",
                }}
              >
                <Form
                  className="contact-form php-mail-form"
                  role="form"
                  action="contactform/contactform.php"
                  method="POST"
                >
                  <div className="form-group">
                    <FormLabel htmlFor="contact-name">Your Name</FormLabel>
                    <input
                      type="name"
                      name="name"
                      className="form-control"
                      id="contact-name"
                      placeholder="Your Name"
                      data-rule="minlen:4"
                      data-msg="Please enter at least 4 chars"
                    />
                    <div className="validate"></div>
                  </div>
                  <div className="form-group">
                    <FormLabel htmlFor="contact-email">Your Email</FormLabel>
                    <input
                      type="email"
                      name="email"
                      className="form-control"
                      id="contact-email"
                      placeholder="Your Email"
                      data-rule="email"
                      data-msg="Please enter a valid email"
                    />
                    <div className="validate"></div>
                  </div>
                  <div className="form-group">
                    <FormLabel htmlFor="contact-subject">Subject</FormLabel>
                    <input
                      type="text"
                      name="subject"
                      className="form-control"
                      id="contact-subject"
                      placeholder="Subject"
                      data-rule="minlen:4"
                      data-msg="Please enter at least 8 chars of subject"
                    />
                    <div className="validate"></div>
                  </div>

                  <div className="form-group">
                    <label htmlFor="contact-message">Your Message</label>
                    <textarea
                      className="form-control"
                      name="message"
                      id="contact-message"
                      placeholder="Your Message"
                      rows="5"
                      data-rule="required"
                      data-msg="Please write something for us"
                    ></textarea>
                    <div className="validate"></div>
                  </div>

                  <div className="loading"></div>
                  <div className="error-message"></div>

                  <div className="form-send">
                    <Button type="submit" className="btn btn-large btn-success">
                      Submit
                    </Button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default ContactUs;
