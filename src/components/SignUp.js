import React from "react";
import SignUpForm from "./SignUpForm";
import "./SignUp.css";

function SignUp() {
  return (
    <>
      {/* <!-- Start Call to Action Section --> */}
      <section id="sign-up" className="sign-up">
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2">
              {/* <div className="pro-block text-center"> */}
              <h1>SIGN UP</h1>
              {/* </div> */}
              <SignUpForm />
            </div>
          </div>
        </div>
      </section>
      {/* <!-- End Call to Action Section --> */}
    </>
  );
}

export default SignUp;
