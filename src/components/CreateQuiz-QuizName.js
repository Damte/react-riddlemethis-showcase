import React, {useState, useEffect} from'react';
function QuizName(props) {
    const [savedEvent, setSavedEvent] = useState("event")
    function handleChange(event) {
        setSavedEvent(event)
        props.onChange(event.target.value, event)
    }

    useEffect(() => {
        props.passEvent(savedEvent)
    }, [savedEvent])

    return (
        <div>
            <div className="row">
                <div className="col-md-2"></div>
                    <div className="col-md-8">
                    <div className="well">
                        <div id="qct-quiz">
                            <div class="form-group">
                                <label for="qct-quiz-name">Quiz Name:</label>
                                <input 
                                    type="text" 
                                    value={props.value}
                                    name="quizName"
                                    class="form-control" 
                                    id="qct-quiz-name" 
                                    placeholder="Please enter the name of your quiz" 
                                    onChange={handleChange}
                                    required="" 
                                    aria-invalid="false"></input>
                                {props.errors.quizName && (<p className="text-danger">{props.errors.quizName}</p>)}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-2"></div>
                </div>
            </div>
        </div>
    )
}
export default QuizName;