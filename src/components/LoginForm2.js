import React from "react";
import useForm from "./validation/useForm2";
import validate from "./validation/CreateQuizValidationRules";

const LoginForm2 = () => {
  const { values, errors, handleChange, handleSubmit } = useForm(
    login,
    validate
  );

  function login() {
    console.log("No errors, submit callback called!");
  }

  return (
    <div className="section is-fullheight">
      <div className="container">
        <div className="column is-4 is-offset-4">
          <div className="box">
            <form onSubmit={handleSubmit} noValidate>
              <div className="field">
                <label className="label">Email Address</label>
                <div className="control">
                  <input
                    autoComplete="off"
                    className={`input ${errors.email && "is-danger"}`}
                    type="email"
                    name="email"
                    onChange={handleChange}
                    value={values.email || ""}
                    required
                  />
                  {errors.email && (
                    <p className="help is-danger">{errors.email}</p>
                  )}
                </div>
              </div>
              <div className="field">
                <label className="label">Password</label>
                <div className="control">
                  <input
                    className={`input ${errors.password && "is-danger"}`}
                    type="password"
                    name="password"
                    onChange={handleChange}
                    value={values.password || ""}
                    required
                  />
                </div>
                {errors.password && (
                  <p className="help is-danger">{errors.password}</p>
                )}
              </div>
              <button
                type="submit"
                className="button is-block is-info is-fullwidth"
              >
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginForm2;

// import React from "react";
// import { useForm } from "react-hook-form";
// import "./LoginForm2.css";

// const LoginForm2 = () => {
//   const { register, handleSubmit, errors } = useForm();

//   function onSubmit(data) {
//     console.log("Data submitted: ", data);
//   }

//   return (
//     <div className="login-form">
//       <form onSubmit={handleSubmit(onSubmit)} noValidate>
//         <label htmlFor="inputEmail">E-mail</label>
//         <input
//           type="email"
//           id="inputEmail"
//           name="email"
//           ref={register({
//             required: "Enter your e-mail",
//             pattern: {
//               value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
//               message: "Enter a valid e-mail address",
//             },
//           })}
//         />
//         {errors.email && <p className="error">{errors.email.message}</p>}

//         <label htmlFor="inputPassword">Password</label>
//         <input
//           type="password"
//           id="inputPassword"
//           name="password"
//           ref={register({ required: "Enter your password" })}
//         />
//         {errors.password && <p className="error">{errors.password.message}</p>}

//         <button type="submit">Login</button>
//       </form>
//     </div>
//   );
// };

// export default LoginForm2;
