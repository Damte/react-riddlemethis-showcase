import React, { useEffect, useState, useRef, useLayoutEffect } from "react";
import DisplayQuestion from "./DisplayQuestion";
import "./DisplayQuiz.css";
import validator from "validator";

function DisplayQuiz() {
  const [quiz, setQuiz] = useState({
    name: "",
    ownerName: "",
    listOfQuestions: [],
  });
  const [UUID, setUUID] = useState([]);
  const [participantEmail, setParticipantEmail] = useState([]);
  const [hasClicked, setHasClicked] = useState([false]);
  const [chosenOptions, setChosenOptions] = useState([]);
  const [answer, setAnswer] = useState({});
  const [isEmailValid, setIsEmailValid] = useState(true);

  const validateEmail = (e) => {
    setParticipantEmail(e.target.value);
    var email = e.target.value;

    if (!validator.isEmail(email) || validator.isEmpty(email)) {
      setIsEmailValid(false);
    } else {
      setIsEmailValid(true);
    }
  };

  const firstUpdate = useRef(true);

  useEffect(() => {
    fetchQuiz();
  }, [UUID]);
  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    } else {
      console.log("JUHUJU");

      setAnswer({
        listOfAnswerDTOs: quiz.listOfQuestions.map((question) => {
          return {
            listOfPostOptionDTOs: question.listOfOptions
              .filter(function (option) {
                return chosenOptions.includes(option.id.toString());
              })
              .map((option) => {
                return { optionId: option.id };
              }),
            questionId: question.id,
          };
        }),
        participantEmail: participantEmail,
        quizId: quiz.id,
      });
      // console.log(myPost)
      // console.log(chosenOptions)
      console.log(answer);
    }
  }, [chosenOptions]);

  const [isChecked, setIsChecked] = useState([]);
  const handleInputChange = (event) => {
    if (event.target.checked === true) {
      setChosenOptions((prevChosenOptions) => [
        ...chosenOptions,
        event.target.name,
      ]);
      console.log("checkedItems: ", chosenOptions);
    }
    if (event.target.checked === false) {
      const newList = chosenOptions.filter(
        (item) => item !== event.target.name
      );

      setChosenOptions(newList);
    }
  };
  const myPost = {
    listOfAnswerDTOs: [
      {
        listOfPostOptionDTOs: [
          {
            optionId: 1,
          },
          {
            optionId: 4,
          },
        ],
        questionId: 1,
      },
      {
        listOfPostOptionDTOs: [
          {
            optionId: 6,
          },
        ],
        questionId: 2,
      },
      {
        listOfPostOptionDTOs: [
          {
            optionId: 7,
          },
        ],
        questionId: 3,
      },
    ],
    participantEmail: participantEmail,
    quizId: quiz.id,
  };

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(answer),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const fetchQuiz = async () => {
    const data = await fetch("http://localhost:8081/quiz/" + UUID);
    const json = await data.json();
    setQuiz(json);
  };
  const handleInput = (event) => {
    setUUID(event.target.value);
  };
  const submitResponseToQuiz = (event) => {
    fetch("http://localhost:8081/quiz/response", optionsJSON)
      .then((res) => {
        if (res.status < 400) {
          alert("Congratulations! Click Ok to continue.");
        }
        return res.json();
      })
      .then((res) => {
        setHasClicked(!hasClicked);
        setUUID("");
        console.log(res);
      });
  };
  const getQuiz = () => {
    // fetchQuiz();
    setHasClicked(!hasClicked);
  };

  const handleQuestionChange = (value) => {
    setChosenOptions(value);
  };

  return (
    <>
      <section id="display-quiz" className="pl-3 buy-pro">
        <div className="container">
          <div id="quiz-form" className="form-group col-9 offset-1">
            <h4 id="quizUUID">
              <label id="quiz-label" htmlFor="email">
                Please enter your Quiz UUID:
              </label>
              <input
                className="ml-3 mr-3 align-text-bottom"
                type="name"
                id="qName"
                value={UUID}
                onChange={handleInput}
                placeholder="Enter uuid"
              />
              <button
                type="submit"
                className="btn btn-outline-success align-text-bottom"
                onClick={getQuiz}
              >
                Get Quiz
              </button>
            </h4>
          </div>
          {hasClicked ? <></> : <h2 id="nameOfQuiz">{quiz.name}</h2>}
          {hasClicked ? (
            <></>
          ) : (
            <h3 id="ownerName">
              {quiz.ownerName}; <h5>Quiz UUID: {UUID}</h5>
            </h3>
          )}
          {hasClicked ? (
            <></>
          ) : (
            <pre>
              {" "}
              <input
                className="mb-2"
                type="email"
                onChange={(e) => validateEmail(e)}
                placeholder="Enter participant mail"
              />
              <div className="text-danger ml-2">
                {isEmailValid ? <></> : <span>Enter valid email</span>}
              </div>
            </pre>
          )}
          {hasClicked ? (
            <></>
          ) : (
            <div>
              {/* {quiz.listOfQuestions.map(question => (<DisplayQuestion question={question} onChange={handleQuestionChange}} />)) */}
              {quiz.listOfQuestions.map((question) => (
                <div className="container">
                  <div className="card border-success mb-3 offset-3 col-6">
                    <div className="card-header">
                      <h5>
                        Question {question.id} {question.description}
                      </h5>
                    </div>
                    <div className="card-body">
                      {question.listOfOptions.map((option) => {
                        return (
                          <div className="text-uppercase text-left">
                            <input
                              className="ml-4 mr-1"
                              type="checkbox"
                              name={option.id}
                              checked={isChecked[option.id]}
                              onChange={handleInputChange}
                            ></input>
                            <label>{option.description}</label>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
          {hasClicked ? (
            <></>
          ) : (
            <div className="text-right">
              <button
                className="btn btn-success align-text-bottom"
                type="submit"
                onClick={submitResponseToQuiz}
              >
                Submit Quiz{" "}
              </button>
            </div>
          )}
        </div>
      </section>
    </>
  );
}

// {
//   "listOfAnswerDTOs": [
//     {
//       "listOfPostOptionDTOs": [
//         {
//           "optionId": 1
//         },
//         {
//           "optionId": 2
//         }
//       ],
//       "questionId": 1
//     },
// {
//       "listOfPostOptionDTOs": [
//         {
//           "optionId": 5
//         }
//       ],
//       "questionId": 2
//     },
// {
//       "listOfPostOptionDTOs": [
//         {
//           "optionId": 7
//         }
//       ],
//       "questionId": 3
//     }
//   ],
//   "participantEmail": "mystring",
//   "quizId": 4
// }

export default DisplayQuiz;
