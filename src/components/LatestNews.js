import React from "react";
import IMG1 from "../images/about-01.jpg";
import IMG2 from "../images/about-02.jpg";
import IMG3 from "../images/about-03.jpg";
import "./LatestNews.css";

function LatestNews() {
  return (
    <>
      {/* <!-- Start Latest News Section --> */}
      <section id="latest-news" className="latest-news-section">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="section-title text-center">
                <h3>Latest News</h3>
                <p>Stay tuned for our last news and developments</p>
              </div>
            </div>
          </div>
          <div className="row latest-news">
            {/* <div className="latest-news col-md-12"> */}
            <div className="col-md-4">
              <div className="latest-post">
                <img src={IMG1} className="img-fluid" alt="img1" />
                <h4>
                  <a href="#">A new beginning</a>
                </h4>
                <div className="post-details">
                  <span className="date">
                    <strong>31</strong> <br />
                    Oct , 2020
                  </span>
                </div>
                <p>Our new adventure here in RMT has started</p>
                <a href="#" className="btn btn-outline-success">
                  Read More
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="latest-post">
                <img src={IMG3} className="img-fluid" alt="img3" />
                <h4>
                  <a href="#">Beta release</a>
                </h4>
                <div className="post-details">
                  <span className="date">
                    <strong>08</strong> <br />
                    Nov , 2020
                  </span>
                </div>
                <p>
                  We realeased our beta version. A lot of hard work was done
                  from all our team
                </p>
                <a href="#" className="btn btn-outline-success">
                  Read More
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="latest-post">
                <img src={IMG2} className="img-fluid" alt="img2" />
                <h4 className="text-success">
                  <a href="#">Security upgrade</a>
                </h4>
                <div className="post-details">
                  <span className="date">
                    <strong>01</strong> <br />
                    Jan , 2021
                  </span>
                </div>
                <p>
                  New features in security added. Now we are implementing jwt
                  security
                </p>
                <a href="#" className="btn btn-outline-success">
                  Read More
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div className="latest-post">
                <img src={IMG2} className="img-fluid" alt="img2" />
                <h4 className="text-success">
                  <a href="#">New features</a>
                </h4>
                <div className="post-details">
                  <span className="date">
                    <strong>01</strong> <br />
                    Mar , 2021
                  </span>
                </div>
                <p>
                  New features have been added such as input validation and
                  graphics showing the quiz results /participation
                </p>
                <a href="#" className="btn btn-outline-success">
                  Read More
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- End Latest News Section --> */}
    </>
  );
}

export default LatestNews;
