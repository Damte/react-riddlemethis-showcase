import React from "react";
import Feature from "./Feature";
import SignUp from "./SignUp";
import FunFacts from "./FunFacts";
import TeamMembers from "./TeamMembers";
import PricingTable from "./PricingTable";
import LatestNews from "./LatestNews";
import TestimonialSection from "./TestimonialSection";
import ContactUs from "./ContactUs";
import LogIn from "./Login";

function MainPage() {
  return (
    <>
      <Feature />
      <SignUp />
      <LogIn />
      <FunFacts />
      <TeamMembers />
      <PricingTable />
      <LatestNews />
      <TestimonialSection />
      <ContactUs />
    </>
  );
}

export default MainPage;
