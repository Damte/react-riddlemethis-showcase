import React from "react";
import "./PricingTable.css";

function PricingTable() {
  return (
    <>
      {/* <!-- Start Pricing Table Section --> */}
      <div id="pricing" className="pricing-section">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="col-md-12">
                <div className="section-title text-center">
                  <h3>Our Pricing Plan</h3>
                  <p className="text-white">
                    According to your choice you will have available the most
                    advanced features and tools
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="pricing col-md-12">
              <div className="row mt-2">
                <div className="col-md-3">
                  <div className="pricing-table">
                    <div className="plan-name">
                      <h3>Free</h3>
                    </div>
                    <div className="plan-price">
                      <div className="price-value">
                        €00<span>.00</span>
                      </div>
                      <div className="interval">per month</div>
                    </div>
                    <div className="plan-list"></div>
                    <div className="plan-signup">
                      <a
                        href="#call-to-action"
                        className="btn-system btn-small"
                      >
                        Sign Up Now
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="pricing-table">
                    <div className="plan-name">
                      <h3>Standard</h3>
                    </div>
                    <div className="plan-price">
                      <div className="price-value">
                        €2<span>.99</span>
                      </div>
                      <div className="interval">per month</div>
                    </div>
                    <div className="plan-list"></div>
                    <div className="plan-signup">
                      <a
                        href="#call-to-action"
                        className="btn-system btn-small"
                      >
                        Sign Up Now
                      </a>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="pricing-table">
                    <div className="plan-name">
                      <h3>Premium</h3>
                    </div>
                    <div className="plan-price">
                      <div className="price-value">
                        €4<span>.99</span>
                      </div>
                      <div className="interval">per month</div>
                    </div>
                    <div className="plan-list"></div>
                    <div className="plan-signup">
                      <a
                        href="#call-to-action"
                        className="btn-system btn-small"
                      >
                        Sign Up Now
                      </a>
                    </div>
                  </div>
                </div>

                <div className="col-md-3">
                  <div className="pricing-table">
                    <div className="plan-name">
                      <h3>Professional</h3>
                    </div>
                    <div className="plan-price">
                      <div className="price-value">
                        €10<span>.00</span>
                      </div>
                      <div className="interval">per month</div>
                    </div>
                    <div className="plan-list"></div>
                    <div className="plan-signup">
                      <a
                        href="#call-to-action"
                        className="btn-system btn-small"
                      >
                        Sign Up Now
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- End Pricing Table Section --> */}
    </>
  );
}

export default PricingTable;
