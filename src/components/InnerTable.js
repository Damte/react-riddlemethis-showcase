import React, { useState, useEffect } from "react";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";
import { Check, X } from "react-bootstrap-icons";

function InnerTable({ questions, uuid }) {
  const [numberOfQuestion, setNumberOfQuestion] = useState([1]);

  useEffect(() => {}, []);

  return (
    <>
      <Table id="my-table-1" className="table table-hover">
        <thead>
          <th scope="col" className="header text-center"></th>
          <th scope="col" className="header text-center"></th>
          <th scope="col"></th>
        </thead>
        <tbody id="table-body1">
          {questions.map((question) => (
            <tr>
              <td>{numberOfQuestion}</td>
              <td>{question.description}</td>
              <td>
                {" "}
                {question.listOfOptionDTOs.map((option) => {
                  return (
                    <li>
                      {option.description}
                      {option.correct ? (
                        <Check className="ml-3" color="DarkGreen" size={35} />
                      ) : (
                        <X className="ml-3" color="Crimson" size={35} />
                      )}
                      {/* <span
                          className={`${option.correct ? "bi bi-check" : ""}`}
                        >
                          {" "}
                        </span> */}
                    </li>
                  );
                })}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <div className="text-center">
        <p>Share this code with your friends so they can complete the quiz!</p>
        <h4>Quiz UUID: {uuid}</h4>
        <Button
          type="submit"
          className="btn btn-large btn-danger"
          style={{ color: "black" }}
        >
          Edit quiz
        </Button>
      </div>
    </>
  );
}

export default InnerTable;
