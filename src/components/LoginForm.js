import React, { useState, useContext } from "react";
import Form from "react-bootstrap/Form";
// import {isAuthenticated} from '../App'
import { Context } from "./IsAuthenticatedContext";

function LoginForm() {
  const [userLogin, setUserLogin] = useState({
    name: "",
    password: "",
  });
  const [jwt, setJwt] = useState({});
  const [id, setId] = useState(0);
  const [context, setContext] = useContext(Context);

  const handleInputChange = (event) => {
    const value = event.target.value;
    setUserLogin({
      ...userLogin,
      [event.target.name]: value,
    });
  };

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(userLogin),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const logIn = async () => {
    const data = await fetch("http://localhost:8081/authenticate", optionsJSON);
    const json = await data.json();
    // setJwt(json);
    // localStorage.setItem('currentUser', JSON.stringify(json));
    //
    console.log(json);
    let toki = json.token;
    let userId = json.userId;
    setJwt(toki);
    setId(userId);
    // console.log(toki)
    // console.log(typeof toki)
    // let key = 'currentUser';
    // let value = toki;

    // value = JSON.stringify(value);

    localStorage.setItem("token", toki);
    if (toki) {
      // setContext("New Value")
      setContext({ ...context, authenticated: true, userId: userId });
    }
  };

  const doLogin = () => {
    logIn();
  };

  return (
    <>
      <div>
        <h1>{context.authenticated}</h1>
        <Form className="text-success text-capitalize text-left ">
          <Form.Group controlId="formGroupEmail">
            <Form.Label>Username:</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter username"
              value={userLogin.name}
              onChange={handleInputChange}
              type="name"
              name="name"
            />
          </Form.Group>
          <Form.Group controlId="formGroupPassword">
            <Form.Label>Password:</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={userLogin.password}
              onChange={handleInputChange}
              type="password"
              name="password"
            />
          </Form.Group>
        </Form>
        <div className="text-right">
          {/* <Button
                        size="lg"
                        variant="outline-success"
                        onClick={doLogin}
                    >Log in</Button>{' '}       */}
          <button onClick={doLogin} className="btn btn-lg btn-outline-success">
            Log in
          </button>
        </div>
      </div>
    </>
  );
}

export default LoginForm;
