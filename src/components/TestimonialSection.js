import React from "react";
import IMG1 from "../images/testimonial/face_1.png";
import IMG2 from "../images/testimonial/face_2.png";
import IMG3 from "../images/testimonial/face_3.png";
import "./TestimonialSection.css";

function TestimonialSection() {
  // RIGHT NOW THERE IS NOT A FUNCTIONAL CAROUSEL, probably it was done in the past with some embedded JQuery
  return (
    <>
      {/* <!-- Start Testimonial Section --> */}
      <div id="testimonial" className="testimonial-section">
        <div className="container">
          {/* <!-- Start Testimonials Carousel --> */}
          <div id="testimonial-carousel" className="testimonials-carousel">
            {/* <!-- Testimonial 1 --> */}
            <div className="testimonials item">
              <div className="testimonial-content">
                <img src={IMG1} alt="img1" />
                <div className="testimonial-author">
                  <div className="author">Mahdad</div>
                  <div className="designation">Organization Founder</div>
                </div>
                <p>
                  It would surely be among the top three final projects that I
                  supervised on SDA
                </p>
              </div>
            </div>
            {/* <!-- Testimonial 2 --> */}
            <div className="testimonials item">
              <div className="testimonial-content">
                <img src={IMG2} alt="" />
                <div className="testimonial-author">
                  <div className="author">William</div>
                  <div className="designation">User</div>
                </div>
                <p>
                  Very intuitive and easy to use. It was fun to create quizzes
                  for my friends
                </p>
              </div>
            </div>
            {/* <!-- Testimonial 3 --> */}
            <div className="testimonials item">
              <div className="testimonial-content">
                <img src={IMG3} alt="img3" />
                <div className="testimonial-author">
                  <div className="author">Carlos Prado</div>
                  <div className="designation">User</div>
                </div>
                <p>
                  Una aplicacion muy bien trabajada, que permite crear contenido
                  de manera dinamica. Muy buenos graficos.
                </p>
              </div>
            </div>
          </div>
          {/* <!-- End Testimonials Carousel --> */}
        </div>
      </div>
      {/* <!-- End Testimonial Section --> */}
    </>
  );
}

export default TestimonialSection;
