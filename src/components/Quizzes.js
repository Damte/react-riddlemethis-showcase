import React, { useState, useEffect, useContext } from "react";
import Quiz from "./EachQuiz";
import { Context } from "./IsAuthenticatedContext";

function Quizzes() {
  const [userId, setUserId] = useState(1);
  const [users, setUsers] = useState([]);
  const [quizzes, setQuizzes] = useState([]);
  //     {
  //     id: 0,
  //     name: "",
  //     userId: 0,
  //     uuid: "",
  //     listOfQuestionDTOs: []
  // }]);
  const [context, setContext] = useContext(Context);

  useEffect(() => {
    setUserId(context.userId);
    fetchUsers();
    // fetch('http://localhost:8081/users')
    //     .then((response) => response.json())
    //     .then((data) => setUsers(data));
    fetchQuizzes();

    //     fetch('http://localhost:8081/user/' + userId + '/quizzes')
    // //http://localhost:8081/user/1/quizzes
    //     .then(response => response.json())
    //     .then(json => setQuizzes(json))
  }, [userId]);
  const fetchUsers = async () => {
    const data = await fetch("http://localhost:8081/users");
    const json = await data.json();
    setUsers(json);
  };

  const fetchQuizzes = async () => {
    let headers = {};
    if (localStorage.token) {
      headers = {
        // 'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.token}`,
      };
    }
    console.log(localStorage.token);
    console.log(headers);
    // const currentUser = localStorage.getItem('currentUser');
    // console.log(currentUser)
    // console.log(typeof currentUser)
    // const token = currentUser.token;
    // console.log(token)
    console.log({ headers: headers });
    const data = await fetch(`http://localhost:8081/user/${userId}/quizzes`, {
      headers: headers,
    });
    const json = await data.json();
    // console.log("json");
    // console.log(json);
    setQuizzes(json);
  };

  return (
    <section>
      {/* {quizzes.map(quiz => (
                <ol><li>{quiz.listOfQuestionDTOs.map(question => {
                    return question.description + "//" + quiz.name;
                })}</li></ol>
            ))} */}

      <div className="container">
        {users.map((user) => {
          if (user.id === userId) {
            return (
              <div className="text-uppercase text-center">
                <h5>List of quizzes created by </h5>
                <h1>{user.name}</h1>
              </div>
            );
          }
        })}

        {quizzes.map((quiz) => (
          <div className="text-center offset-3">
            <Quiz
              key={quiz.id}
              name={quiz.name}
              listOfQuestions={quiz.listOfQuestionDTOs}
              uuid={quiz.uuid}
            />
          </div>
        ))}
      </div>
    </section>
  );
}

export default Quizzes;
