import React, { useState } from "react";
import "./DisplayQuestion.css";

function DisplayQuestion(props) {
  const [options, setOptions] = useState([]);
  const [isChecked, setIsChecked] = useState([]);
  const [checkedItems, setCheckedItems] = useState({}); //plain object as state
  const handleInputChange = (event) => {
    setOptions({ ...options, [event.target.name]: event.target.true });
    console.log("checkedItems: ", options);
    props.onChange(options.filter);
  };

  return (
    <>
      <div className="container">
        <div class="card border-success mb-3 offset-3 col-6">
          <div class="card-header">
            <h5>
              Question {props.question.id} {props.question.description}
            </h5>
          </div>
          <div class="card-body">
            {props.question.listOfOptions.map((option) => {
              return (
                <div className="text-uppercase text-left">
                  <input
                    className="ml-4 mr-1"
                    type="checkbox"
                    name={option.id}
                    checked={isChecked[option.id]}
                    onChange={handleInputChange}
                  ></input>
                  <label>{option.description}</label>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default DisplayQuestion;
