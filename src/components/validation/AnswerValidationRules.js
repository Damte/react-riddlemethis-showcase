export default function validate(values) {
    let errors = {};
        if (!values.ansDes) {
            errors.ansDes = 'Question is required';
        } else if (!/^The|A/.test(values.ansDes)) {
            errors.ansDes = 'Please enter a better answer';
        }

    return errors;
};