import { useState, useEffect } from 'react';
const useCreateQuizForm = (callback, validate) => {
    const [values, setValues] = useState([{}]);
    const [errors, setErrors] = useState([{}]);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [userInput, setUserInput] = useState({
        id: "",
        name: "",
        value: ""
    });
    const[index, setIndex] = useState(-1);
    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
            callback();
        }
    }, [errors]);
    useEffect(() => {
       
        if (index === -1) { 
            setValues([...values, userInput])
        } else {
            values.splice(index, 1, userInput)
            let newValues = JSON.parse(JSON.stringify(values));
            setValues(newValues)
        }
    }, [userInput]);

   
    const handleSubmit = (event) => {
        if (event) event.preventDefault();
        setErrors(validate(values));
        setIsSubmitting(true);
    };
    // const handleChange = (event) => {
    //     event.persist();
    //     setValues(values => ({ ...values, [event.target.name]: event.target.value }));
    // };
    // const handleSubmit = (event) => {
    //     if (event) event.preventDefault();
    //     setErrors(validate(values));
    //     setIsSubmitting(true);
    // };
    const handleChange = (event) => {
        event.persist();
        let newIndex = values.findIndex(value => value.id === event.target.id)
        setIndex(newIndex)
        setUserInput({ id: event.target.id, name: event.target.name, value: event.target.value })

        // setValues(values => ({ ...values, [event.target.name]: event.target.value }));
    };
    return {
        handleChange,
        handleSubmit,
        values,
        errors,
    }
};
export default useCreateQuizForm;

// import { useState, useEffect } from 'react';

// const useCreateQuizForm = (callback, validate, numOfAns) => {

//     const [values, setValues] = useState({});
//     const [errors, setErrors] = useState({});
//     const [isSubmitting, setIsSubmitting] = useState(false);

//     useEffect(() => {
//         if (Object.keys(errors).length === 0 && isSubmitting) {
//             callback();
//         }
//     }, [errors]);

//     const handleSubmit = (event) => {
//         if (event) event.preventDefault();
//         setErrors(validate(values, numOfAns));
//         setIsSubmitting(true);
//     };

//     const handleChange = (nameValue) => {
       
//         let name = nameValue.name
//         let value = nameValue.value
//         // setValues(nameValue)
//         setValues(values => ({ ...values, [nameValue.name]: nameValue.value }));
//     };

//     return {
//         handleChange,
//         handleSubmit,
//         values,
//         errors,
//     }

// };

// export default useCreateQuizForm;