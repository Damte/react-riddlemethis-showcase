export default function validate(values) {
    let errors = [];
    for (let i = 0; i < values.length; i++) {
        let value = values[i];
        if (value.name === "quizName") {
            if (value.value === "") {
            errors.push({id: value.id, errorDescription: "Quiz name is required"});}
        } else if (value.name === "quesDes") {
            if (value.value === "") {
                errors.push({id: value.id, errorDescription: "Question is required"})
            }
        } else if (value.name === "ansDes") {
            if (value.value === "") {
                errors.push({id: value.id, errorDescription: "Answer is required"})
            }
        } 
    }


return errors;
};