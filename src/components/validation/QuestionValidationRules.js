export default function validate(values) {
    let errors = {};
        if (!values.quesDes) {
            errors.quesDes = 'Question is required';
        } else if (!/^The|a/.test(values.quesDes)) {
            errors.quesDes = 'Please enter a better question';
        }

    return errors;
};
