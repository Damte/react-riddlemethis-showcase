export default function validate(values) {
    let errors = {};
    if (!values.quizName) {
        errors.quizName = 'Title is required';
    } else if (/[a-z]+/.test(values.quizName)) {
        errors.quizName = 'Title is invalid';
    }
    // if (!values.quesDes) {
    //     errors.quesDes = 'Question is required';
    // } else if (!/^The|a/.test(values.quesDes)) {
    //     errors.quesDes = 'Please enter a better question';
    // }
    // if (!values.ansDes) {
    //     errors.ansDes = 'Answer is required';
    // } else if (!/^The|a/.test(values.quesDes)) {
    //     errors.ansDes = 'Answer is invalid';
    // }
    return errors;
};