import React, { useState, useContext } from "react";
// import {isAuthenticated} from '../App'
import { Context } from "./IsAuthenticatedContext";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import "./NavbarComponent.css";
import "./green.css";
// import '../css/style.css'
// import '../css/color/green.css'
// import '../css/animate.css'
// import '../css/font-awesome.css'
// import '../css/font-awesome.min.css'
// import '../css/owl.carousel.css'
// import '../css/owl.transitions.css'
// import '../css/responsive.css'
// import '../css/quiz-display.css'

function NavbarComponent(props) {
  const [loggedIn, setLoggedIn] = useState(false);
  const [context, setContext] = useContext(Context);

  const quizzesButton = !context.authenticated ? (
    <></>
  ) : (
    <Link to="/quizzes">
      <li>
        <a className="page-scroll">Quizzes</a>
      </li>
    </Link>
  );
  const createQuizButton = !context.authenticated ? (
    <></>
  ) : (
    <Link to="/create-quiz">
      <li>
        <a className="page-scroll">Create Quiz</a>
      </li>
    </Link>
  );
  const logInButton = !context.authenticated ? (
    <li>
      <a className="page-scroll" href="#log-in">
        Log In
      </a>
    </li>
  ) : (
    <></>
  );
  return (
    <>
      {/* <!-- Navigation --> */}
      <Nav
        className="navbar navbar-default  fixed-top navbar-dark b"
        id="navbar-component"
      >
        {/* <h1 style={{color: "red"}}>{ context.authenticated}</h1> */}
        {/* <h1> isAuthenticated</h1> */}
        <div className="container">
          {/* <!-- Brand and toggle get grouped for better mobile display --> */}
          <div className="navbar-header page-scroll">
            <Button
              type="button"
              className="navbar-toggle"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1"
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </Button>
            <Link to="/">
              <a className="navbar-brand page-scroll">RMT</a>
            </Link>
          </div>

          {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
          <div
            className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1"
          >
            <ul className="nav navbar-nav navbar-right">
              <li className="hidden">
                <a href="#page-top"></a>
              </li>
              <li>
                <a className="page-scroll" href="#feature">
                  Feature
                </a>
              </li>
              <li>
                <a className="page-scroll" href="#sign-up">
                  Sign Up
                </a>
              </li>
              {logInButton}
              {/* <li>
						<a className="page-scroll" href="#log-in">Log In</a>
					</li> */}

              <li>
                <a className="page-scroll" href="#team">
                  Team
                </a>
              </li>
              <li>
                <a className="page-scroll" href="#pricing">
                  Pricing
                </a>
              </li>
              <li>
                <a className="page-scroll" href="#latest-news">
                  Latest News
                </a>
              </li>
              <li>
                <a className="page-scroll" href="#testimonial">
                  Testimonials
                </a>
              </li>
              <Link to="/quiz">
                <li>
                  <a className="page-scroll">Display Quiz</a>
                </li>
              </Link>
              {quizzesButton}
              {/* <Link to='/quizzes' >
							<li>		
						<a className="page-scroll">Quizzes</a>
							</li>
						</Link> */}
              {createQuizButton}
              {/* <Link to='/create-quiz' >
					<li>
						<a className="page-scroll" >Create Quiz</a>
							</li>
							</Link> */}
              <li>
                <a className="page-scroll" href="#contact">
                  Contact
                </a>
              </li>
            </ul>
          </div>
          {/* <!-- /.navbar-collapse --> */}
        </div>
        {/* <!-- /.container-fluid --> */}
      </Nav>
    </>
  );
}
export default NavbarComponent;
