import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import InnerTable from "./InnerTable";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";

function EachQuiz({ name, listOfQuestions, uuid }) {
  // function CarouselCard(props) {
  const [responses, setResponses] = useState([]);
  const userId = 3;

  const fetchResponses = async () => {
    let headers = {};
    if (localStorage.token) {
      headers = {
        Authorization: `Bearer ${localStorage.token}`,
      };
    }
    const data = await fetch(
      `http://localhost:8081/user/ ${userId} /quizzes/responses`,
      { headers: headers }
    );
    // https://riddle-me-this-quizapp.herokuapp.com/user/
    //http://localhost:8081/user/1/quizzes/responses
    // ('https://riddle-me-this-quizapp.herokuapp.com/quiz/' + quizId + '/count-quiz-responses')
    const json = await data.json();
    setResponses(json);
  };

  useEffect(() => {
    fetchResponses();
  }, []);

  return (
    <>
      {/* <h1> <ol><li>{listOfQuestions.map(question => {
                return question.description + "//";
                })}</li></ol></h1> */}
      <Accordion defaultActiveKey="0">
        <Card>
          <Button
            id="Button-number-4"
            className="btn btn-success btn-lg btn-block"
            data-toggle="collapse"
            data-target="#Card-body-1"
            aria-expanded="true"
            aria-controls="Card-body-1"
          >
            <Accordion.Toggle as={Card.Header} eventKey="0">
              <span className="text-uppercase display-5">{name}</span>

              <div className="row" id="quiz4row1">
                <span className="badge badge-light text-right" id="badge-4">
                  Completed{" "}
                  {responses.map((response) => {
                    if (response.quizName === name)
                      return "#" + response.numberOfParticipantResponses;
                  })}{" "}
                  times
                </span>
              </div>
              <div id="Button-container-1" className="Accordion-button">
                <div className="form-send text-right"></div>
              </div>
            </Accordion.Toggle>
          </Button>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <InnerTable questions={listOfQuestions} uuid={uuid} />
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
      <div className="card">
        <div className="card-header">
          <h5 className="mb-0"></h5>
        </div>
      </div>
    </>
  );
}
export default EachQuiz;
