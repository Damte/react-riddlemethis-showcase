import React from "react";
import "./TeamMembers.css";
import DMG from "../images/team/manage-1.png";
import Brendan from "../images/team/manage-2.png";
import Stanley from "../images/team/manage-3.png";
import Dmitri from "../images/team/manage-4.png";

function TeamMembers() {
  return (
    <>
      {/* <!-- Start Team Member Section --> */}
      <section id="team" class="team-member-section">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="section-title text-center">
                <h3>Our Team</h3>
                <p>Meet our development team</p>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              {/* <div class="container"> */}
              <div id="team-section">
                <div class="our-team">
                  <div class="team-member">
                    <img
                      src={DMG}
                      class="img-fluid col-md-2 offset-md-1"
                      alt="mb-1"
                    />
                    <div class="team-details">
                      <h4>David Miguel Gaite Cruz</h4>
                      <p>Backend & Frontend developer</p>
                      <ul>
                        <li>
                          <a href="#">
                            <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-linkedin"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-pinterest"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-dribbble"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div class="team-member">
                    <img src={Brendan} class="img-fluid col-md-2" alt="mb-2" />
                    <div class="team-details">
                      <h4>Brendan Koch</h4>
                      <p>Backend & Frontend developer</p>
                      <ul>
                        <li>
                          <a href="#">
                            <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-linkedin"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-pinterest"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-dribbble"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div class="team-member">
                    <img src={Stanley} class="img-fluid col-md-2" alt="mb-3" />
                    <div class="team-details">
                      <h4>Stanley Ezeoke</h4>
                      <p>Contributor</p>
                      <ul>
                        <li>
                          <a href="#">
                            <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-linkedin"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-pinterest"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-dribbble"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div class="team-member">
                    <img src={Dmitri} class="img-fluid col-md-2" alt="mb-4" />
                    <div class="team-details">
                      <h4>Dmitri Marinen</h4>
                      <p>Contributor</p>
                      <ul>
                        <li>
                          <a href="#">
                            <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-linkedin"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-pinterest"></i>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-dribbble"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- End Team Member Section --> */}
    </>
  );
}

export default TeamMembers;
