import React from "react";
import LoginForm from "./LoginForm";
import LoginForm2 from "./LoginForm2";
import "./Login.css";

function LogIn() {
  return (
    <>
      <section id="log-in" className="log-in">
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2">
              <h1 className=" text-uppercase">Log In</h1>
              <LoginForm />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default LogIn;
