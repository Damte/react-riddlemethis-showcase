import React, { useState, useEffect } from "react";
import useDidMountEffect from "./useDidMountEffect";
import useCreateQuizForm from "./validation/useForm2";
import validate from "./validation/AnswerValidationRules";
function CreateAnswer({
  answerNum,
  qNum,
  onChange,
  onInputChange,
  onQuestionTypeChange,
  errors,
}) {
  console.log(`answer render`);
  const [isDisabled, setIsDisabled] = useState(false);
  const [savedEvent, setSavedEvent] = useState("saved");
  const [optionDTO, setOptionDTO] = useState({
    correct: false,
    description: "",
    // answerNum: answerNum
  });
  // const {
  //     values,
  //     errors,
  //     handleChange,
  //     handleSubmit,
  // } = useCreateQuizForm(enterData, validate);
  function enterData() {
    console.log("No errors, submit callback called!");
  }

  // ------------- Function for toggeling disable on answer inputs when question type changed

  useDidMountEffect(() => {
    setIsDisabled(answerNum > 2 ? onQuestionTypeChange : false);
  }, [onQuestionTypeChange]);

  // ------------- Functions for updating DTO on input change
  useDidMountEffect(() => {
    console.log(`Ans useEffect in Question ${JSON.stringify(optionDTO)}`);
    onChange(optionDTO, answerNum);
  }, [optionDTO]);

  useDidMountEffect(() => {
    // onInputChange(savedEvent, answerNum)
    onInputChange(savedEvent);
  }, [savedEvent]);
  function handleAnsDesChange(event) {
    let descr = event.target.value;
    setOptionDTO({ ...optionDTO, description: descr });
    setSavedEvent(event);
    // handleChange(event)
    console.log(`handleDesChange ${JSON.stringify(optionDTO)}`);
  }
  function handleCBInputChange(event) {
    setOptionDTO({ ...optionDTO, correct: event.target.checked });
  }

  // ------------- ------------- ------------- ------------- ------------- ------------- -------------

  const root = `qct-q${qNum}-a${answerNum}-`;
  const contId = `${root}cont`;
  const desLabelId = `${root}des-label`;
  const ansTypeId = `${root}des-type`;
  const inputDesId = `${root}des`;

  const answerError =
    typeof errors.find((error) => error.id === inputDesId) == "undefined" ? (
      <></>
    ) : (
      <>{errors.find((error) => error.id === inputDesId).errorDescription}</>
    );

  return (
    <div
      id="qct-q1-ansRef"
      aria-labelledby="collapse"
      data-parent="#qct-q1-des-label"
    >
      <div className="qct-answer" id={contId}>
        <div className="pr-4">
          <div className="col-md-4">
            <label htmlFor={desLabelId}>Answer {answerNum}:</label>
          </div>
          <div className="col-md-2"></div>
          <label htmlFor={ansTypeId} className="mt-4">
            Correct Answer
          </label>
          <input
            type="checkbox"
            // id={ansTypeId}
            value="true"
            style={{ marginLeft: "5px" }}
            onChange={handleCBInputChange}
            disabled={isDisabled}
          ></input>
        </div>
        <div className="form-group">
          <input
            type="text"
            // value= {value}
            id={inputDesId}
            name="ansDes"
            className="form-control"
            required=""
            placeholder="Please enter an answer to the question"
            style={{ width: "96%", marginLeft: "2%" }}
            onChange={handleAnsDesChange}
            // onBlur={handleSubmit}
            disabled={isDisabled}
          ></input>
          {answerError}
        </div>
      </div>
    </div>
  );
}
export default CreateAnswer;
// function AnswerCreationBlock() {
//     return (
//         <div id="qct-q1-ansRef" className="collapse in" aria-labelledby="collapse" data-parent="#qct-q1-des-label">
//             <div className="qct-answer" id="qct-q1-a1-cont">
//                 <div className="pr-4">
//                     <div className="col-md-4">
//                         <label for="qct-q1-a1-des">Answer 1:</label>
//                     </div>
//                     <div className="col-md-2"></div>
//                     <label for="qct-q1-a1-type" className="mt-4">Correct Answer</label>
//                     <input
//                         type="checkbox"
//                         id="qct-q1-a1-type"
//                         value="true"
//                         style={{marginLeft: "5px"}}
//                     ></input>
//                 </div>
//                 <div className="form-group">
//                     <input
//                         type="text"
//                         id="qct-q1-a1-des"
//                         className="form-control"
//                         required=""
//                         placeholder="Please enter an answer to the question"
//                         style={{width: "96%", marginLeft: "2%"}}
//                     ></input>
//                 </div>
//             </div>
//         </div>
//     )
// }
