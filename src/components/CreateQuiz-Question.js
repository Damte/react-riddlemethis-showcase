import React, { useState, useEffect } from "react";
import CreateAnswer from "./CreateQuiz-Answer";
import useDidMountEffect from "./useDidMountEffect";
import Button from "react-bootstrap/Button";
import useCreateQuizForm from "./validation/useForm";
import validate from "./validation/CreateQuizValidationRules2";
function CreateQuestion(props) {
  console.log(`question render`);
  const [ansNum, setAnsNum] = useState(3);
  const [ansDTOs, setAnsDTOs] = useState([]);
  const [isBinary, setIsBinary] = useState(true);
  const [questionType, setQuestionType] = useState("BINARY");
  const [questDescription, setQuestDescription] = useState("");
  const [savedEvent, setSavedEvent] = useState("savedEvent");
  const [buttonRemAnsIsDisabled, setButtonRemAnsIsDisabled] = useState({});
  const [buttonAddAnsIsDisabled, setButtonAddAnsIsDisabled] = useState({});
  // ------------- David, please add an explinantion here: I think I get it, but not 100%

  const [questTypeRadioButt, setQuestTypeRadioButt] = useState({
    state: "BINARY",
  });

  const [answers, setAnswers] = useState([1, 2]);
  const { values, errors, handleChange, handleSubmit } = useCreateQuizForm(
    enterData,
    validate
  );
  function enterData() {
    console.log("No errors, submit callback called!");
  }

  // ------------- State for the Question DTO

  const [questDTO, setQuestDTO] = useState({
    description: questDescription,
    listOfOptionDTOs: [],
    questionType: questionType,
    // questionNum: props.questionNum
  });
  // -------------  Updates the DTO whenever changes are made to its properties
  useDidMountEffect(() => {
    // props.onChange(questDTO, props.questionNum, savedEvent)
    // props.onChange(questDTO, props.questionNum)
    console.log(`Quest useEffect in Question ${JSON.stringify(questDTO)}`);
  }, [questDTO]);
  // -------------  Updates the desciption property in the Quiz DTO on user input
  function handleDesChange(event) {
    let descr = event.target.value;
    setSavedEvent(`${event.target.name} +  ${event.target.value}`);
    setSavedEvent(event);
    setQuestDTO({ ...questDTO, description: descr });
    console.log(`handleDesChange ${JSON.stringify(questDTO)}`);
    // props.onChange(questDTO, props.questionNum, event)
    let propertyName = `${[event.target.name]}-${props.questionNum}`;
    handleChange(event);
  }
  // -------------  Updates the Ans DTO in the Quiz DTO when changes are made in the Answer component
  // when question type is set to binary, only the first two answers are used.
  useDidMountEffect(() => {
    console.log(`handleAnsDTOsChange ${JSON.stringify(questDTO)}`);
    setQuestDTO({
      ...questDTO,
      listOfOptionDTOs: isBinary ? ansDTOs.slice(0, 2) : ansDTOs,
      questionType: questionType,
    });
  }, [ansDTOs, questionType]);
  function handleAnswerChange(newDTO, ansNum, savedAnsEvent) {
    // function handleAnswerChange(newDTO, ansNum) {
    let currentAnswerNumber = ansNum - 1;
    if (ansDTOs.length < currentAnswerNumber) {
      ansDTOs.push({});
      currentAnswerNumber++;
    }
    ansDTOs.splice(currentAnswerNumber, 1, newDTO);
    let newAnsDTOs = JSON.parse(JSON.stringify(ansDTOs));
    setAnsDTOs(newAnsDTOs);
    setSavedEvent(savedAnsEvent);
    // handleSubmit();
  }
  function handleInputChange(inputChangeEvent) {
    setSavedEvent(inputChangeEvent);
    let propertyName = `${[inputChangeEvent.target.name]}-${ansNum}`;
    // handleChange({
    //     name: propertyName,
    //     value: inputChangeEvent.target.value
    // });
    handleChange(inputChangeEvent);
  }
  // useDidMountEffect(() => {
  //     handleSubmit();
  // }, [props.hasSubmitted])

  // -------------  Adds an answer component and the Ans DTO in the Quiz DTO when changes are made in the Answer component

  const addAnswer = (event) => {
    setAnsNum(ansNum + 1);
    console.log("added q: " + props.questionNum + " a: " + ansNum);
    ansDTOs.push({}); // might be better to have this here??
    setAnswers(answers.concat(ansNum));
  };

  // ------------- Function for the 'Add Answer' button, disables the button when the the question type is set to Binary

  useEffect(() => {
    setButtonAddAnsIsDisabled(isBinary);
  }, [isBinary]);

  // ------------- Function for the 'Remove Answer' button, disables the button when there are less than 3 answer components

  useEffect(() => {
    setButtonRemAnsIsDisabled(ansNum <= 3);
  }, [ansNum]);
  function remAnswer() {
    setAnsNum(ansNum - 1);
    console.log(
      "removed q: " + props.questionNum + " a: " + (ansNum.valueOf() - 1)
    );
    answers.pop();
    setAnswers(answers);
  }

  // ------------- Functions for changing question type on User input. Changes question type in DTO, isBinary and Radio button 'checked' state.

  function handleQuestTypeRadioInputChange() {
    var newQuestType = isBinary ? "MULTI" : "BINARY";
    setIsBinary((prev) => !prev);
    setQuestTypeRadioButt({ ...questTypeRadioButt, state: newQuestType });
    setQuestionType(newQuestType);
  }

  // ------------- ------------- ------------- ------------- ------------- ------------- -------------

  const root = `qct-q${props.questionNum}-`;
  const contId = `${root}cont`;
  const desLabelId = `${root}des-label`;
  const binaryLabelId = `${root}type-binary`;
  const multiLabelId = `${root}type-multi`;
  const inputDesId = `${root}des`;
  const buttonAddAnsId = `${root}addAns`;
  const buttonRemAnsId = `${root}remAns`;
  const questionError =
    typeof errors.find((error) => error.id === inputDesId) == "undefined" ? (
      <></>
    ) : (
      <>{errors.find((error) => error.id === inputDesId).errorDescription}</>
    );
  return (
    <div className="qct-answer" id={contId}>
      <div className="row">
        <div className="col-md-2"></div>
        <div className="col-md-8">
          <div className="well">
            <div className="row">
              <div className="col-md-4">
                <label
                  className="h3"
                  htmlFor={inputDesId}
                  id={desLabelId}
                  data-toggle="collapse"
                  data-target="#qct-q1-ansRef"
                  aria-expanded="true"
                  aria-controls="collapseOne"
                >
                  Question: {props.questionNum}
                </label>
              </div>
              <form>
                <span className="form-group col-md-2">
                  <label htmlFor="qct-q1-type-binary">True/False</label>
                  <input
                    className="form-check-input"
                    type="radio"
                    id={binaryLabelId}
                    name="qct-q1-type"
                    value="BINARY"
                    checked={questTypeRadioButt.state === "BINARY"}
                    onChange={handleQuestTypeRadioInputChange}
                    style={{ marginLeft: "5px" }}
                  ></input>
                </span>
                <span className="col-md-2">
                  <label htmlFor="qct-q1-type-multi">Multi-choice</label>
                  <input
                    className="form-check-input"
                    type="radio"
                    id={multiLabelId}
                    name="qct-q1-type"
                    value="MULTI"
                    checked={questTypeRadioButt.state === "MULTI"}
                    onChange={handleQuestTypeRadioInputChange}
                    style={{ marginLeft: "5px" }}
                  ></input>
                </span>
              </form>
            </div>
            <div className="row form-group" style={{ marginBottom: "25px" }}>
              <input
                type="text"
                value={values.quesDes}
                name="quesDes"
                id={inputDesId}
                className="form-control"
                required=""
                placeholder="Please enter the question"
                onChange={handleDesChange}
                // onBlur={handleSubmit}
                style={{ width: "96%", marginLeft: "2%" }}
              ></input>
              {/* { &&(<p className="text-danger pl-3">{errors.find(error => error.id === inputDesId).id}</p>)} */}
              {questionError}
            </div>
            {answers.map((answerNum) => (
              <div>
                {
                  <CreateAnswer
                    key={answerNum}
                    answerNum={answerNum}
                    qNum={props.questionNum}
                    onChange={handleAnswerChange}
                    onQuestionTypeChange={isBinary}
                    onInputChange={handleInputChange}
                    errors={errors}
                  />
                }
              </div>
            ))}
            <div>
              <Button
                variant="primary"
                onClick={addAnswer}
                size="md"
                disabled={buttonAddAnsIsDisabled}
              >
                Add answer
              </Button>
              <button
                className="btn btn-primary"
                id={buttonRemAnsId}
                onClick={remAnswer}
                disabled={buttonRemAnsIsDisabled}
              >
                Remove answer
              </button>
              {/* This button is only here in order to test how the validation could be implemented with only two levels. Will be removed once the whole validation process has been put in place. */}
              <div>
                <Button variant="primary" onClick={handleSubmit} size="md">
                  provisional submit button
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CreateQuestion;
