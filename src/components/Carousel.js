import React from "react";
import HeaderBackground from "../images/header-bg-1.jpg";
import "font-awesome/css/font-awesome.min.css";
import "./Carousel.css";

function Carousel() {
  return (
    <div>
      {/* <!-- Start Home Page Slider --> */}
      <section id="page-top">
        {/* <!-- Carousel --> */}
        <div id="main-slide" className="carousel slide" data-ride="carousel">
          {/* <!-- Indicators --> */}
          <ol className="carousel-indicators">
            <li
              data-target="#main-slide"
              data-slide-to="0"
              className="active"
            ></li>
            <li data-target="#main-slide" data-slide-to="1"></li>
            <li data-target="#main-slide" data-slide-to="2"></li>
          </ol>
          {/* <!--/ Indicators end--> */}

          {/* <!-- Carousel inner --> */}
          <div className="carousel-inner">
            <div className="item active">
              <img
                className="img-responsive"
                src={HeaderBackground}
                alt="slider"
              />
              <div className="slider-content">
                <div className="col-md-12 text-center">
                  <h1 className="animated3">
                    <span>
                      Ask <strong>questions</strong> with RMT
                    </span>
                  </h1>
                  <p className="animated2">
                    Create your own quizzes, polls or surveys for sharing with
                    your friends and colleagues
                  </p>
                  <a
                    href="#feature"
                    className="page-scroll btn btn-primary animated1"
                  >
                    Read More
                  </a>
                </div>
              </div>
            </div>
            {/* <!--/ Carousel item end --> */}
            {/* <!--  old img cant be seen in firefox src="images//galaxy.jpg" --> */}
            <div className="item">
              <img
                className="img-responsive"
                src={HeaderBackground}
                alt="slider"
              />
              <div className="slider-content">
                <div className="col-md-12 text-center">
                  <h1 className="animated2">
                    <span>
                      Step in and <strong>get started</strong>
                    </span>
                  </h1>
                  <p className="animated1">
                    Sign up, create and <br /> share your customized quizzes
                  </p>
                  <a
                    className="animated3 slider btn btn-primary btn-min-block"
                    href="#call-to-action"
                  >
                    Sign Up
                  </a>
                  <a
                    className="animated3 slider btn btn-default btn-min-block"
                    href="#"
                  >
                    Login
                  </a>
                </div>
              </div>
            </div>
            {/* <!--/ Carousel item end --> */}
          </div>
          {/* <!-- Carousel inner end--> */}

          {/* <!-- Controls --> */}
          <a
            className="left carousel-control"
            href="#main-slide"
            data-slide="prev"
          >
            <span>
              <i className="fa fa-angle-left"></i>
            </span>
          </a>
          <a
            className="right carousel-control"
            href="#main-slide"
            data-slide="next"
          >
            <span>
              <i className="fa fa-angle-right"></i>
            </span>
          </a>
        </div>
        {/* <!-- /carousel --> */}
      </section>
      {/* <!-- End Home Page Slider --> */}
    </div>

    //    </div>
  );
}

export default Carousel;
