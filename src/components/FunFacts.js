import React from "react";
import "./FunFacts.css";

function FunFacts() {
  return (
    <>
      {/* <!-- Start Fun Facts Section --> */}
      <section className="fun-facts">
        <div className="container">
          <div className="row">
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="counter-item">
                <i className="fa fa-question" aria-hidden="true"></i>
                <div
                  className="timer"
                  id="item1"
                  data-to="991"
                  data-speed="5000"
                >
                  30
                </div>
                <h5>Quizzes created</h5>
              </div>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="counter-item">
                <i className="fa fa-check"></i>
                <div
                  className="timer"
                  id="item2"
                  data-to="7394"
                  data-speed="5000" //Right now is not working, before it was working with JQuery,
                  //but we should find a solution either by bringing the JQuery here or by solving it with JS/React
                >
                  90
                </div>
                <h5>Responses submitted</h5>
              </div>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="counter-item">
                <i className="fa fa-pie-chart" aria-hidden="true"></i>
                <div
                  className="timer"
                  id="item3"
                  data-to="18745"
                  data-speed="5000"
                >
                  10
                </div>
                <h5>Surveys created</h5>
              </div>
            </div>
            <div className="col-xs-12 col-sm-6 col-md-3">
              <div className="counter-item">
                <i className="fa fa-male"></i>
                <div
                  className="timer"
                  id="item4"
                  data-to="8423"
                  data-speed="5000"
                >
                  60
                </div>
                <h5>Number of users</h5>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <!-- End Fun Facts Section --> */}
    </>
  );
}

export default FunFacts;
