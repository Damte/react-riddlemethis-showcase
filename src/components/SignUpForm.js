import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
// import './SignUp.css'

function SignUpForm() {
  const [user, setUser] = useState({
    name: "",
    email: "",
    password: "",
  });
  const handleInputChange = (event) => {
    const value = event.target.value;
    setUser({
      ...user,
      [event.target.name]: value,
    });
  };

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    },
  };

  function saveNewUser(event) {
    fetch("http://localhost:8081/user", optionsJSON)
      .then((res) => {
        if (res.status < 400) {
          alert("Congratulations! Click Ok to continue.");
          console.log("res");
          console.log(res);
        }
        return res.json();
      })
      .then((res) => {
        console.log("res de res");
        console.log(res);
      });
  }
  return (
    <div>
      <form className="contact-form" onSubmit={saveNewUser}>
        <div className="form-group">
          <label
            className="d-block"
            style={{ color: "white", textAlign: "left" }}
          >
            Your Name:
            <input
              className="form-control"
              value={user.name}
              onChange={handleInputChange}
              type="name"
              name="name"
              placeholder="Please enter your Name"
            />
          </label>
        </div>
        <div className="form-group">
          <label
            className="d-block"
            style={{ color: "white", textAlign: "left" }}
          >
            Your Email:
            <input
              className="form-control"
              value={user.email}
              onChange={handleInputChange}
              type="email"
              name="email"
              placeholder="Please enter your Email"
            />
          </label>
        </div>
        <div className="form-group">
          <label
            className="d-block"
            style={{ color: "white", textAlign: "left" }}
          >
            Password:
            <input
              className="form-control"
              value={user.password}
              onChange={handleInputChange}
              type="password"
              name="password"
              placeholder="Please enter your Password"
            />
          </label>
        </div>
        <div className="text-right">
          {/* <input
            type="submit"
            // onClick={saveNewUser}
            className="btn btn-lg btn-light"
            value="Sign Up!"
          /> */}
        </div>
      </form>
      <div className="text-right">
        <button onClick={saveNewUser} className="btn btn-lg btn-light">
          Sign up
        </button>
      </div>
    </div>
    // <form
    //   className="contact-form text-right"
    //   onSubmit={saveNewUser}

    // >
    //   <div className="form-group" style={{ color: "white", textAlign: "left" }}>
    //     <label for="user-name">Your Name</label>
    //     <input
    //       value={user.name}
    //       onChange={handleInputChange}
    //       type="name"
    //       name="name"
    //       className="form-control"
    //       id="user-name"
    //       placeholder="Please enter your Name"

    //     />
    //     <div className="validate"></div>
    //   </div>
    //   <div className="form-group" style={{ color: "white", textAlign: "left" }}>
    //     <label for="user-email">Your Email</label>
    //     <input
    //       value={user.email}
    //       onChange={handleInputChange}
    //       type="email"
    //       name="email"
    //       className="form-control"
    //       id="user-email"
    //       placeholder="Please enter your Email"
    //     />
    //     <div className="validate"></div>
    //   </div>
    //   <div className="form-group" style={{ color: "white", textAlign: "left" }}>
    //     <label for="user-subject">Password</label>
    //     <input
    //       value={user.password}
    //       onChange={handleInputChange}
    //       type="password"
    //       name="password"
    //       className="form-control"
    //       id="user-password"
    //       placeholder="Please enter your Password"
    //     />
    //     <div className="validate"></div>
    //   </div>

    //   <div className="loading"></div>
    //   <div className="error-message"></div>

    //   <div className="form-send">
    //     <input
    //       type="submit"
    //       className="btn btn-lg btn-light"
    //     //   style={{ color: "black" }}
    //       value="Sign Up!"
    //     />
    //       {/* Sign Up! */}
    //     {/* </button> */}
    //   </div>
    // </form>
  );
}

export default SignUpForm;
