import React, { useState } from 'react'
import { Context } from "./components/IsAuthenticatedContext";


import './App.css';
import NavbarComponent from './components/NavbarComponent'
import Carousel from './components/Carousel'
import NewCarousel from './components/NewCarousel'
import Quizzes from './components/Quizzes';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import DisplayQuiz from './components/DisplayQuiz';
import CreateQuiz from './components/CreateQuiz-QuizName';
import MainPage from './components/MainPage';
import QuizCreationTool from './components/CreateQuiz-Tool'

export const isAuthenticated = React.createContext();

// export const useIsAuthenticated = () => useContext(hasBeenAuthenticated);
  

const hasBeenAuthenticated = 
{
  authenticated: false
}


function App() {
  const [context, setContext] = useState(
    {
      authenticated: false,
      userId: 0
    }
  );
  return (
    <div className="App">
      <Router>
          <Context.Provider value={[context, setContext]}>
          <NavbarComponent />
          <Carousel/>
        <Switch>
            <Route path="/" exact component={MainPage} />
        <Route path="/quizzes" component={ Quizzes }/>
        <Route path="/quiz" component={ QuizCreationTool }/>
        <Route path="/create-quiz" component={ DisplayQuiz }/>
        </Switch>
            </Context.Provider>
        </Router>

    </div>
  );
}

export default App;
